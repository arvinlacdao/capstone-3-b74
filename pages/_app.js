import React, { useState, useEffect } from 'react';
// Import global css
import '../styles/global.css';
// Import CSS Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import NavBar from '../components/NavBar';
// Import Context Provider
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';

export default function App({ Component, pageProps }) {

    // State hook for user state, define here for global scope 
    const [user, setUser] = useState({
        // Initialized as an object with properties set as null
        // Proper values will be obtained from localStorage AFTER component gets rendered due to Next.JS pre-rendering
        id: null,
        isAdmin: null
    })

    console.log(user);

    // Function for clearing local storage upon logout
    const unsetUser = () => {
        localStorage.clear();
        // Set the user global scope in the context provider to have its email set to null
        setUser({
            id: null,
            isAdmin: null
        });
    }

    // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            if (typeof data._id !== 'undefined') {
                setUser({ id: data._id, isAdmin: data.isAdmin })
            } else {
                setUser({ id: null, isAdmin: null })   
            }
        })
    }, [user.id])

    // Effect hook for testing the setUser() functionality
    useEffect(() => {
        console.log(user.id);
    }, [user.id])

    return (
        <React.Fragment>
            {/* Wrap the component tree within the UserProvider context provider so that components will have access to the passed in values here */}
            <UserProvider value={{user, setUser, unsetUser}}>
                <NavBar />
                <Container>
                    <Component {...pageProps} />
                </Container>
            </UserProvider>
        </React.Fragment>
    );
}